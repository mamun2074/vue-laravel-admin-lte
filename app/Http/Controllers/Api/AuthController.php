<?php

namespace App\Http\Controllers\Api;

use App\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{

    /**
     * Register Api User
     */

    public function register(Request $request)
    {

        $request->validate([
            'name'=>'required',
            'email'=>'required | string | email | max:255 | unique:users',
            'password'=>'required | string | min:2 | confirmed'
        ]);

        $user=User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=>Hash::make($request->password),
        ]);
        $accessToken=$user->createToken('authToken')->accessToken;

        return response()->json([
           'user'=>$user,
            'access_token'=>$accessToken,
        ]);

    }

    /*
     * Login Api User
     * */

    public function login(Request $request)
    {

        $userData=$request->validate([
            'email'=>'required | string | email | max:255',
            'password'=>'required'
        ]);

        if( !auth()->attempt($userData) ){
            return response()->json([
                'message'=>'Email and Password Does Not match'
            ], 401);
        }

        $loginToken = auth()->user()->createToken('authToken')->accessToken;
        return response()->json([
            'message'=>'Successfully Log in',
            'user'=>auth()->user(),
            'access_token'=>$loginToken,
        ] , 200);

    }
}
