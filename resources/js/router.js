
import dashboard from './components/dashboard.vue';
import settings from './components/settings/settings.vue';
import profile from './components/profile/profile.vue';
import pageNotFound from './components/page-not-found.vue';

export const routes = [
    {
        path: '/admin/dashboard',
        component: dashboard ,
        name: 'dashboard'
    },
    {
        path: '/admin/settings',
        component: settings,
        name: 'settings'
    },
    {
        path: '/admin/profile',
        component: profile,
        name: 'profile'
    },
    {
        path: '*',
        component: pageNotFound,
        name: 'page-not-found'

    }

];